﻿namespace Kalkulator
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ip = new System.Windows.Forms.MaskedTextBox();
            this.mask = new System.Windows.Forms.MaskedTextBox();
            this.network = new System.Windows.Forms.MaskedTextBox();
            this.broadcast = new System.Windows.Forms.MaskedTextBox();
            this.firstIp = new System.Windows.Forms.MaskedTextBox();
            this.lastIp = new System.Windows.Forms.MaskedTextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mask:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "Network:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "Broadcast:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 14);
            this.label5.TabIndex = 4;
            this.label5.Text = "First IP:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "Last IP:";
            // 
            // ip
            // 
            this.ip.Location = new System.Drawing.Point(92, 6);
            this.ip.Mask = "000\\.000\\.000\\.000";
            this.ip.Name = "ip";
            this.ip.Size = new System.Drawing.Size(100, 20);
            this.ip.TabIndex = 6;
            this.ip.Text = "192168000001";
            this.ip.TextChanged += new System.EventHandler(this.Recalculate);
            // 
            // mask
            // 
            this.mask.Location = new System.Drawing.Point(92, 30);
            this.mask.Mask = "000\\.000\\.000\\.000";
            this.mask.Name = "mask";
            this.mask.Size = new System.Drawing.Size(100, 20);
            this.mask.TabIndex = 7;
            this.mask.Text = "255255255000";
            this.mask.TextChanged += new System.EventHandler(this.Recalculate);
            // 
            // network
            // 
            this.network.Location = new System.Drawing.Point(92, 54);
            this.network.Mask = "000\\.000\\.000\\.000";
            this.network.Name = "network";
            this.network.ReadOnly = true;
            this.network.Size = new System.Drawing.Size(100, 20);
            this.network.TabIndex = 8;
            this.network.Text = "192168000000";
            // 
            // broadcast
            // 
            this.broadcast.Location = new System.Drawing.Point(92, 78);
            this.broadcast.Mask = "000\\.000\\.000\\.000";
            this.broadcast.Name = "broadcast";
            this.broadcast.ReadOnly = true;
            this.broadcast.Size = new System.Drawing.Size(100, 20);
            this.broadcast.TabIndex = 9;
            this.broadcast.Text = "192168000255";
            // 
            // firstIp
            // 
            this.firstIp.Location = new System.Drawing.Point(92, 102);
            this.firstIp.Mask = "000\\.000\\.000\\.000";
            this.firstIp.Name = "firstIp";
            this.firstIp.ReadOnly = true;
            this.firstIp.Size = new System.Drawing.Size(100, 20);
            this.firstIp.TabIndex = 10;
            this.firstIp.Text = "192168000001";
            // 
            // lastIp
            // 
            this.lastIp.Location = new System.Drawing.Point(92, 126);
            this.lastIp.Mask = "000\\.000\\.000\\.000";
            this.lastIp.Name = "lastIp";
            this.lastIp.ReadOnly = true;
            this.lastIp.Size = new System.Drawing.Size(100, 20);
            this.lastIp.TabIndex = 11;
            this.lastIp.Text = "192168000254";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 189);
            this.Controls.Add(this.lastIp);
            this.Controls.Add(this.firstIp);
            this.Controls.Add(this.broadcast);
            this.Controls.Add(this.network);
            this.Controls.Add(this.mask);
            this.Controls.Add(this.ip);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(225, 194);
            this.Name = "Form1";
            this.Text = "Kalkulator IP";
            ((System.ComponentModel.ISupportInitialize) (this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.ErrorProvider errorProvider1;

        private System.Windows.Forms.MaskedTextBox mask;
        private System.Windows.Forms.MaskedTextBox network;
        private System.Windows.Forms.MaskedTextBox broadcast;
        private System.Windows.Forms.MaskedTextBox firstIp;
        private System.Windows.Forms.MaskedTextBox lastIp;

        private System.Windows.Forms.MaskedTextBox ip;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;

        #endregion
    }
}

