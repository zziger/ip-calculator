﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        private readonly Regex _validator = new Regex(@"\d{3}\.\d{3}\.\d{3}\.\d{1,3}", RegexOptions.Compiled);
        private readonly Regex _formatter = new Regex(@"(?<=^|\.)0+(?!\.|$)", RegexOptions.Compiled);
        private readonly Regex _reverseFormatter1 = new Regex(@"(?<=^|\.)(\d{1})(?=$|\.)", RegexOptions.Compiled);
        private readonly Regex _reverseFormatter2 = new Regex(@"(?<=^|\.)(\d{2})(?=$|\.)", RegexOptions.Compiled);
        
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            throw new NotImplementedException();
        }

        string FormatReverse(IPAddress address)
        {
            var strAddr = address.ToString();
            return _reverseFormatter2.Replace(_reverseFormatter1.Replace(strAddr, "00$1"), "0$1");
        }

        void ClearFields()
        {
            network.Text = "000000000000";
            broadcast.Text = "000000000000";
            firstIp.Text = "000000000000";
            lastIp.Text = "000000000000";
        }

        private void Recalculate(object sender, EventArgs e)
        {
            errorProvider1.SetError(ip, _validator.IsMatch(ip.Text) ? "" : "Invalid IP");
            errorProvider1.SetError(mask, _validator.IsMatch(mask.Text) ? "" : "Invalid mask");
            if (!_validator.IsMatch(ip.Text) || !_validator.IsMatch(mask.Text))
            {
                ClearFields();
                return;
            }

            var formattedIp = _formatter.Replace(ip.Text, "");
            var formattedMask = _formatter.Replace(mask.Text, "");

            IPNetwork ipObject;
            try
            {
                 ipObject = IPNetwork.Parse(formattedIp, formattedMask);
            }
            catch (ArgumentException exception)
            {
                switch (exception.Message)
                {
                    case "netmask":
                        errorProvider1.SetError(mask, "Invalid mask");
                        break;
                    case "ipaddress":
                        errorProvider1.SetError(ip, "Invalid IP");
                        break;
                }
                return;
            }

            network.Text = FormatReverse(ipObject.Network);
            broadcast.Text = FormatReverse(ipObject.Broadcast);
            firstIp.Text = FormatReverse(ipObject.FirstUsable);
            lastIp.Text = FormatReverse(ipObject.LastUsable);
        }
    }
}